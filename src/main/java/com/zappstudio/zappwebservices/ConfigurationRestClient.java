package com.zappstudio.zappwebservices;

import android.support.annotation.Nullable;

import com.google.gson.JsonElement;
import com.zappstudio.zappwebservices.exception.BaseException;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.logging.HttpLoggingInterceptor;


public interface ConfigurationRestClient {
     @Nullable Map<String, String> getHeadersInterception();
     @Nullable BaseException checkErrorGeneric(JsonElement response);
     @Nullable BaseException checkHttpError(int code, @Nullable String body);
     @Nullable Map<String,String> getCertificatePinner();
     JsonElement extractBodyJsonElement(JsonElement response);
     String getBasepath();
     HttpLoggingInterceptor.Level getLevelLog();
     TimeUnit getTimeoutTimeUnit();
     int getTimeOutTime();
     JsonElement bodyTreatment(JsonElement body);
     Map<String, String> bodyTreatment(Map<String, String> params);
}
