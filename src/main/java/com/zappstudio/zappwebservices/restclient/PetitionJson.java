package com.zappstudio.zappwebservices.restclient;

import android.content.Context;

import com.google.gson.JsonElement;
import com.zappstudio.zappwebservices.ConfigurationRestClient;
import com.zappstudio.zappwebservices.model.TypedFile;

import java.util.Map;

/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
@SuppressWarnings("unused")
public class PetitionJson extends RestClientGeneric implements RestClientInterface {

    private final JsonElement param;

    private PetitionJson(Builder builder) {
        super(builder);
        param = builder.param;
    }


    @Override
    public void wsSuccess(JsonElement jsonElement) {
        callbackGeneric.success(jsonElement);
    }

    @Override
    public void wsFailure(Exception e) {
        callbackGeneric.error(e);
    }


    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public Map<String, TypedFile> getFiles() {
        return null;
    }

    @Override
    public JsonElement getJsonElement() {
        return param;
    }

    //RestClientInterface
    @Override
    public void makePetition(){
        setPOSTJson();
    }


    public static final class Builder extends BuilderGenericPetition<Builder>  {

        private JsonElement param;

        public Builder(String endpoint, CallbackGeneric callbackGeneric, Context context, ConfigurationRestClient configurationRestClient, JsonElement param) {
            super(endpoint, callbackGeneric, context, configurationRestClient);
            this.param = param;
        }

        @Deprecated
        public Builder() {

        }

        public Builder param(JsonElement val) {
            param = val;
            return this;
        }

        public RestClientInterface build() {
            if(checkParameters() && param != null){
                return new PetitionJson(this);
            }else{
                throw new IllegalStateException();
            }
        }
    }

    //Dont use
    @Override
    public Map<String, String> getParamsString() {
        return null;
    }


}
