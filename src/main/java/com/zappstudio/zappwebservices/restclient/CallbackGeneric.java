package com.zappstudio.zappwebservices.restclient;

import com.google.gson.JsonElement;


/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
public interface CallbackGeneric {
    void success(JsonElement jsonElement);
    void error(Exception e);
}
