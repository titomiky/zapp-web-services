package com.zappstudio.zappwebservices.restclient;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.JsonElement;
import com.zappstudio.zappwebservices.ConfigurationRestClient;
import com.zappstudio.zappwebservices.exception.BaseException;
import com.zappstudio.zappwebservices.exception.MTimeoutException;
import com.zappstudio.zappwebservices.exception.NetworkNotConnectionException;
import com.zappstudio.zappwebservices.exception.ServerErrorNotControled;

import java.util.Map;


/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
public abstract class RestClientGeneric extends RestClient {

    private ConfigurationRestClient configurationRestClient;
    protected final String endpoint;
    protected final CallbackGeneric callbackGeneric;
    protected final Context context;

    public RestClientGeneric(BuilderGenericPetition builder) {
        super(builder);
        this.configurationRestClient = builder.getConfigurationRestClient();
        this.endpoint = builder.getEndpoint();
        this.callbackGeneric = builder.getCallbackGeneric();
        this.context = builder.getContext();
    }

    /*
    * inform to child
    */
    public abstract void wsSuccess(JsonElement jsonElement);

    public abstract void wsFailure(Exception e);

    @Override
    protected final void wsGenericSucessfully(JsonElement jsonElement) {
        BaseException baseException = configurationRestClient.checkErrorGeneric(jsonElement);
        if(baseException!=null){
            wsFailure(baseException);
        }else{
            wsSuccess(configurationRestClient.extractBodyJsonElement(jsonElement));
        }
    }

    @Override
    protected final void wsGenericError(int code, @Nullable String body) {
        BaseException baseException;
        switch (code) {
            case RestClient.CODE_CONNECT_EXCEPTION:
                baseException = new NetworkNotConnectionException();
                break;
            case RestClient.CODE_TIMEOUT_EXCEPTION:
                baseException = new MTimeoutException();
                break;
            default:
                baseException = configurationRestClient.checkHttpError(code, body);
                if (baseException == null)
                    baseException = new ServerErrorNotControled();
                break;
        }
        wsFailure(baseException);
    }


    /*
    * Todas las peticiones pasaran por aqui para devolver los tokens genericos
    */
    @Nullable
    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headersInterception = configurationRestClient.getHeadersInterception();
        return headersInterception;
    }
}
