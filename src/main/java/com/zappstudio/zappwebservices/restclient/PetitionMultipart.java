package com.zappstudio.zappwebservices.restclient;

import android.content.Context;

import com.google.gson.JsonElement;
import com.zappstudio.zappwebservices.ConfigurationRestClient;
import com.zappstudio.zappwebservices.model.TypedFile;

import java.util.HashMap;
import java.util.Map;


/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
@SuppressWarnings("unused")
public class PetitionMultipart extends RestClientGeneric implements RestClientInterface{

    private final Map<String, String> params;
    private final Map<String, TypedFile> files;

    public PetitionMultipart(Builder builder) {
        super(builder);
        params = builder.params;
        files = builder.files;
    }

    @Override
    public void wsSuccess(JsonElement jsonElement) {
        callbackGeneric.success(jsonElement);
    }

    @Override
    public void wsFailure(Exception e) {
        callbackGeneric.error(e);
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public Map<String, TypedFile> getFiles() {
        return files;
    }

    @Override
    public Map<String, String> getParamsString() {
        return params;
    }

    @Override
    public void makePetition(){
        setMultipart();
    }


    public static final class Builder extends BuilderGenericPetition<Builder>{
        private Map<String, String> params;
        private Map<String, TypedFile> files;

        public Builder(String endpoint, CallbackGeneric callbackGeneric, Context context, ConfigurationRestClient configurationRestClient) {
            super(endpoint, callbackGeneric, context, configurationRestClient);
            params = new HashMap<>();
            files = new HashMap<>();
        }

        @Deprecated
        public Builder() {
            params = new HashMap<>();
            files = new HashMap<>();
        }

        public Builder setParam(String key, String value) {
            params.put(key, value);
            return this;
        }

        public Builder setFiles(String key, TypedFile typedFile) {
            files.put(key, typedFile);
            return this;
        }

        public RestClientInterface build() {
            if(checkParameters()){
                return new PetitionMultipart(this);
            }else{
                throw new IllegalStateException();
            }
        }
    }


    //Dont use
    @Override
    public JsonElement getJsonElement() {
        return null;
    }
}
