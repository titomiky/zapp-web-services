package com.zappstudio.zappwebservices.restclient;

import android.content.Context;

import com.google.gson.JsonElement;
import com.zappstudio.zappwebservices.ConfigurationRestClient;
import com.zappstudio.zappwebservices.model.TypedFile;

import java.util.HashMap;
import java.util.Map;


/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
@SuppressWarnings("unused")
public class PetitionFormUrl extends RestClientGeneric implements RestClientInterface{

    private final Map<String, String> params;

    public PetitionFormUrl(Builder builder) {
        super(builder);
        params = builder.params;
    }


    @Override
    public void wsSuccess(JsonElement jsonElement) {
        callbackGeneric.success(jsonElement);
    }

    @Override
    public void wsFailure(Exception e) {
        callbackGeneric.error(e);
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public Map<String, String> getParamsString() {
        return params;
    }

    public void makePetition(){
        setPOSTPetition();
    }

    public static final class Builder extends BuilderGenericPetition<Builder>{
        private Map<String, String> params;

        public Builder(String endpoint, CallbackGeneric callbackGeneric, Context context, ConfigurationRestClient configurationRestClient, Map<String, String> params) {
            super(endpoint, callbackGeneric, context, configurationRestClient);
            this.params = params;
        }

        @Deprecated
        public Builder() {
            params = new HashMap<>();
        }

        public Builder setParam(String key, String value) {
            params.put(key, value);
            return this;
        }

        public RestClientInterface build() {
            if(checkParameters()){
                return new PetitionFormUrl(this);
            }else{
                throw new IllegalStateException();
            }
        }
    }



    //Dont use
    @Override
    public Map<String, TypedFile> getFiles() {
        return null;
    }
    @Override
    public JsonElement getJsonElement() {
        return null;
    }

}
