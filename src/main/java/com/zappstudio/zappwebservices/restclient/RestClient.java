package com.zappstudio.zappwebservices.restclient;


import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.zappstudio.zappwebservices.ConfigurationRestClient;
import com.zappstudio.zappwebservices.cache.PetitionCache;
import com.zappstudio.zappwebservices.model.Typed;
import com.zappstudio.zappwebservices.model.TypedFile;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import okhttp3.CertificatePinner;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class RestClient {

    private static final String TAG = RestClient.class.getSimpleName();
    static final int CODE_TIMEOUT_EXCEPTION = 1889;
    static final int CODE_CONNECT_EXCEPTION = 54767;

    private ApiService apiService;
    private BuilderGenericPetition builder;
    private ConfigurationRestClient configurationRestClient;

    public RestClient(BuilderGenericPetition builder) {
        this.builder = builder;
        configurationRestClient = builder.getConfigurationRestClient();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(configurationRestClient.getLevelLog());

        CertificatePinner.Builder pinnerBuilder = new CertificatePinner.Builder();
        Map<String, String> certificatePinner = configurationRestClient.getCertificatePinner();

        if (certificatePinner != null) {
            for (Object o : certificatePinner.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                pinnerBuilder.add(pair.getValue() + "", pair.getKey() + "");
            }
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor())
                .addInterceptor(loggingInterceptor)
                .connectTimeout(configurationRestClient.getTimeOutTime(), configurationRestClient.getTimeoutTimeUnit())
                .readTimeout(configurationRestClient.getTimeOutTime(), configurationRestClient.getTimeoutTimeUnit())
                .writeTimeout(configurationRestClient.getTimeOutTime(), configurationRestClient.getTimeoutTimeUnit())
                .certificatePinner(pinnerBuilder.build())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(configurationRestClient.getBasepath())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    /*
     * Informa al hijo de que el ws fue bien y le da su response
     */
    protected abstract void wsGenericSucessfully(JsonElement jsonElement);


    /*
    * Informa al hijo de que el ws fue mal
    */
    protected abstract void wsGenericError(int code, @Nullable String body);


    /*
    * Pide el endpoint al hijo
    */
    public abstract String getEndpoint();


    public abstract Map<String, TypedFile> getFiles();

    public abstract Map<String, String> getParamsString();
    public abstract JsonElement getJsonElement();


    private Map<String, String> getParamsMergedString() {
        Map<String, String> params = getParamsString();
        if (params != null)
            return configurationRestClient.bodyTreatment(params);
        return null;
    }


    private JsonElement getParamsMergedJson() {
        JsonElement param = getJsonElement();
        if (param != null)
            return configurationRestClient.bodyTreatment(param);
        return null;
    }


    /*
        * Metodo para pedir las headers extras al hijo asi como token geolocalizacion etc
        */
    public abstract
    @Nullable
    Map<String, String> getHeaders();

    /*
    * Post petition con parametro Json
    */
    final void setPOSTPetition() {
        Call<JsonElement> call = apiService.setPOSTPetition(getEndpoint(), getParamsMergedString());
        makeRequest(call);
    }


    /*
     * Post petition con parametro Json
     */
    final void setMultipart() {
        Map<String, RequestBody> params = new HashMap<>();

        Map<String, String> paramsString = getParamsMergedString();
        if (paramsString != null) {
            for (Object o : paramsString.entrySet()) {
                Entry pair = (Entry) o;
                params.put((String) pair.getKey(), RequestBody.create(Typed.getMediaTyped(Typed.TEXT), (String) pair.getValue()));
            }
        }

        Map<String, TypedFile> paramsFile = getFiles();
        if (paramsFile != null) {
            for (Object f : paramsFile.entrySet()) {
                Entry pair = (Entry) f;
                TypedFile value = (TypedFile) pair.getValue();
                String key = (String) pair.getKey();
                params.put(key + "\"; filename=\"" + value.getFile().getName() + "\"", RequestBody.create(Typed.getMediaTyped(value.getMediaType()), value.getFile()));
            }
        }

        Call<JsonElement> call = apiService.setMultipart(getEndpoint(), params);
        makeRequest(call);
    }


    /*
    * Post petition con parametro Json
    */
    final void setPOSTJson() {
        Call<JsonElement> call = apiService.setPOSTJson(getEndpoint(), getParamsMergedJson());
        makeRequest(call);
    }

    /*
    * Post petition con parametro Json
    */
    final void setPUTJson() {
        Call<JsonElement> call = apiService.setPUTJson(getEndpoint(), getParamsMergedJson());
        makeRequest(call);
    }

    /*
    * Get petition con parametros String
    */
    final void setGETPetition() {
        Call<JsonElement> call = apiService.setGETPetition(getEndpoint(), getParamsMergedString());
        makeRequest(call);
    }

    /*
    * Get petition con parametros String
    */
    final void setDELETEPetition() {
        Call<JsonElement> call = apiService.setDELETEPetition(getEndpoint(), getParamsMergedString());
        makeRequest(call);
    }

    /*
    * Aqui se realiza la peticion de forma sincrona
    */
    private void makeRequest(Call<JsonElement> calling) {

        String responseCached = null;
        if (!builder.isForcePetitionOverCache())
            responseCached = PetitionCache.getInstance().getPetition(builder.getContext(), getEndpoint(), getParamsMergedString(), getParamsMergedJson(), builder.getTimeExpired());

        if (responseCached != null) {
            try {
                wsGenericSucessfully(new JsonParser().parse(responseCached));
            } catch (JsonParseException e) {
                e.printStackTrace();
            }
        } else {
            try {
                retrofit2.Response<JsonElement> execute = calling.execute();
                if (execute.isSuccessful()) {
                    PetitionCache.getInstance().savePetition(builder.getContext(), getEndpoint(), getParamsMergedString(), getParamsMergedJson(), execute.body());
                    wsGenericSucessfully(execute.body());
                } else {
                    wsGenericError(execute.code(), execute.errorBody().string());
                }
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                wsGenericError(CODE_TIMEOUT_EXCEPTION, "TIMEOUT_EXCEPTION");
            } catch (ConnectException | UnknownHostException e) {
                e.printStackTrace();
                wsGenericError(CODE_CONNECT_EXCEPTION, "CONNECT_EXCEPTION");
            } catch (InterruptedIOException e) {
                Log.d(TAG, "Thread is disposed");
            } catch (IOException e) {
                e.printStackTrace();
                wsGenericError(0, "UNEXPECTED_ERROR");
            }
        }
    }


    /*
    * Interceptor de headers se pediran a los hijos
    */
    private class HeaderInterceptor
            implements Interceptor {
        @Override
        public Response intercept(Chain chain)
                throws IOException {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();
            Map<String, String> headers = getHeaders();
            if (headers != null) {
                for (Object o : headers.entrySet()) {
                    Entry pair = (Entry) o;
                    builder.addHeader(pair.getKey() + "", pair.getValue() + "");
                }
            }
            request = builder.build();
            return chain.proceed(request);
        }
    }


}