package com.zappstudio.zappwebservices.restclient;

import com.google.gson.JsonElement;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;


public interface ApiService {

    @FormUrlEncoded
    @POST("/{endpoint}")
    Call<JsonElement> setPOSTPetition(@Path(value = "endpoint", encoded = true) String endpoint, @FieldMap Map<String, String> params);

    @GET("/{endpoint}")
    Call<JsonElement> setGETPetition(@Path(value = "endpoint", encoded = true) String endpoint, @QueryMap Map<String, String> options);

    @Multipart
    @POST("/{endpoint}")
    Call<JsonElement> setMultipart(@Path(value = "endpoint", encoded = true) String endpoint, @PartMap Map<String, RequestBody> params);

    @POST("{endpoint}")
    Call<JsonElement> setPOSTJson(@Path(value = "endpoint", encoded = true) String endpoint, @Body JsonElement body);

    @PUT("/{endpoint}")
    Call<JsonElement> setPUTJson(@Path(value = "endpoint", encoded = true) String endpoint, @Body JsonElement jsonElement);

    @DELETE("/{endpoint}")
    Call<JsonElement> setDELETEPetition(@Path(value = "endpoint", encoded = true) String endpoint, @QueryMap Map<String, String> options);


}