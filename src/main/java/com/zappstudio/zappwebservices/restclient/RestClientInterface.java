package com.zappstudio.zappwebservices.restclient;

/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
public interface RestClientInterface {
     void makePetition();
}
