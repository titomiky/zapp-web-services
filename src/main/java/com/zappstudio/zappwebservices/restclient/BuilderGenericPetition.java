package com.zappstudio.zappwebservices.restclient;

import android.content.Context;
import android.util.Log;

import com.zappstudio.zappwebservices.ConfigurationRestClient;


public abstract class BuilderGenericPetition<T> {

    public static final String TAG = "BuilderGeneric.class";
    public static final String ERROR_PARAMETERS = "Algun parametro obligatorio no se encuentra";

    private String endpoint;
    private CallbackGeneric callbackGeneric;
    private boolean isCached = false;
    private boolean forcePetitionOverCache = false;
    private long timeExpired;
    private Context context;
    private ConfigurationRestClient configurationRestClient;

    public BuilderGenericPetition(String endpoint, CallbackGeneric callbackGeneric, Context context, ConfigurationRestClient configurationRestClient) {
        this.endpoint = endpoint;
        this.callbackGeneric = callbackGeneric;
        this.context = context;
        this.configurationRestClient = configurationRestClient;
    }

    @Deprecated
    public BuilderGenericPetition() {
    }

    public T  endpoint(String val) {
        endpoint = val;
        return (T) this;
    }

    public T  callbackGeneric(CallbackGeneric val) {
        callbackGeneric = val;
        return (T) this;
    }

    /**
     * Cachea la respuesta de la petición
     * @param timeExpired Tiempo de expiración del cacheado en milisegundos
     */
    public T cache(long timeExpired) {
        this.isCached = true;
        this.timeExpired = timeExpired;
        return (T) this;
    }

    /**
     * Se fuerza a realizar la petición aunque ya esté cacheada su respuesta
     * @param timeExpired Tiempo de expiración del cacheado en milisegundos
     */
    public T cacheForcePetition(long timeExpired) {
        this.forcePetitionOverCache = true;
        return cache(timeExpired);
    }

    public T context(Context val) {
        context = val;
        return (T) this;
    }

    public T configuration(ConfigurationRestClient val) {
        configurationRestClient = val;
        return (T) this;
    }

    public boolean checkParameters(){
        if(endpoint == null || callbackGeneric == null || context == null || configurationRestClient == null){
            Log.e(TAG,ERROR_PARAMETERS);
            return false;
        }
        return true;
    }

    public ConfigurationRestClient getConfigurationRestClient() {
        return configurationRestClient;
    }

    public CallbackGeneric getCallbackGeneric() {
        return callbackGeneric;
    }

    public Context getContext() {
        return context;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public boolean isForcePetitionOverCache() {
        return forcePetitionOverCache;
    }

    public boolean isCached() {
        return isCached;
    }

    public long getTimeExpired() {
        return timeExpired;
    }
}
