package com.zappstudio.zappwebservices.exception;

/*
 *
 * Created by Cristian Menárguez González By Zapp Studio
 *
 */
public class NetworkNotConnectionException extends BaseException{

    public NetworkNotConnectionException() {
        super();
    }

}
