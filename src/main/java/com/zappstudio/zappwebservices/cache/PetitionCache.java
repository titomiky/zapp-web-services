package com.zappstudio.zappwebservices.cache;

import android.content.Context;

import com.google.gson.JsonElement;
import com.zappstudio.zappwebservices.cache.database.DBCache;

import java.security.MessageDigest;
import java.util.Map;

/**
 * Created by puesto7 on 4/5/16.
 */
public class PetitionCache {

    private static PetitionCache instance;

    public static PetitionCache getInstance() {
        if (instance == null)
            instance = new PetitionCache();
        return instance;
    }

    public void clearCache(Context context) {
        DBCache.getInstance(context).eraseDatabase();
    }

    public String getPetition(Context context, String endpoint, Map<String, String> paramsMap, JsonElement jsonElement, long timeExpired){
        String petitionKey = getPetitionKey(endpoint, paramsMap, jsonElement);
        return DBCache.getInstance(context).getResponse(petitionKey, timeExpired);
    }

    public void savePetition(Context context, String endpoint, Map<String, String> paramsMap, JsonElement jsonElement, JsonElement response){
        String petitionKey = getPetitionKey(endpoint, paramsMap, jsonElement);
        DBCache.getInstance(context).save(petitionKey, response.toString());
    }

    private String getPetitionKey(String endpoint, Map<String, String> paramsMap, JsonElement jsonElement) {
        String key = endpoint;
        if (paramsMap != null && !paramsMap.isEmpty()) {
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                key = key.concat(entry.getKey() + "-" + entry.getValue());
            }
        }
        if (jsonElement != null) {
            key = key.concat(jsonElement.toString());
        }
        return stringToMD5(key);
    }

    private static String stringToMD5(String text) {
        try {
            MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(text.getBytes());
            byte[] bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(String.format("%02X", aByte));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return "";
        }
    }

}
