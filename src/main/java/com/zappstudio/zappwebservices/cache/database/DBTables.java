package com.zappstudio.zappwebservices.cache.database;

/**
 * Created by PuestoUno on 26/01/15.
 */
public class DBTables {

    public static class TDownloadedPath {
        public static final String TABLENAME = "downloadedpath";

        public static final String PETITION = "petition";
        public static final String RESPONSE = "response";
        public static final String TIME_EXPIRED = "time_expired";

        public static final String CREATETABLE = "CREATE TABLE " + TABLENAME + " (" +
                PETITION + " VARCHAR PRIMARY KEY, " +
                RESPONSE + " VARCHAR, " +
                TIME_EXPIRED + " INTEGER);";

        public static final String DROPTABLE = "DROP TABLE IF EXISTS " + TABLENAME;
    }

}
