package com.zappstudio.zappwebservices.cache.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by PuestoUno on 26/01/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "petition_cache.sqlite";
    private static final int DB_VERSION = 1;
    private SQLiteDatabase database;
    private static DBHelper dbHelper;

    public static DBHelper getInstance(Context context){
        if (dbHelper == null) dbHelper = new DBHelper(context);
        return dbHelper;
    }

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    public SQLiteDatabase open(Context context){
        if (database == null){
            database = getInstance(context).getWritableDatabase();
        }
        return database;
    }

    public void close(){
        if (database != null){
            database.close();
            database = null;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        if (sqLiteDatabase.isReadOnly()) sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(DBTables.TDownloadedPath.CREATETABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL(DBTables.TDownloadedPath.DROPTABLE);
        onCreate(sqLiteDatabase);
    }
}
