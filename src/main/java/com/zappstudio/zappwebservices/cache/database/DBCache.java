package com.zappstudio.zappwebservices.cache.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by PuestoUno on 03/12/15.
 */
public class DBCache {

    private static DBCache instance;
    private SQLiteDatabase database;

    private DBCache(Context context) {
        database = DBHelper.getInstance(context).open(context);
    }

    public static DBCache getInstance(Context context) {
        if (instance == null) instance = new DBCache(context);
        return instance;
    }

    public void save(String petition, String response){
        database.beginTransaction();
        database.insertWithOnConflict(DBTables.TDownloadedPath.TABLENAME, null, getDownloadPathCV(petition, response, System.currentTimeMillis()), SQLiteDatabase.CONFLICT_REPLACE);
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public void eraseDatabase(){
        database.beginTransaction();
        database.delete(DBTables.TDownloadedPath.TABLENAME,null,null);
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public String getResponse(String petition, long timeExpired){
        String response = null;
        Cursor cursor = database.query(DBTables.TDownloadedPath.TABLENAME, new String[]{DBTables.TDownloadedPath.RESPONSE}, DBTables.TDownloadedPath.PETITION + "=? AND " + DBTables.TDownloadedPath.TIME_EXPIRED + ">?", new String[]{petition, String.valueOf(System.currentTimeMillis() - timeExpired)}, null, null, null, "1");
        if (cursor.moveToFirst())
            response = cursor.getString(cursor.getColumnIndex(DBTables.TDownloadedPath.RESPONSE));
        cursor.close();
        return response;
    }


    private ContentValues getDownloadPathCV(String petition, String response, long timeExpired){
        ContentValues cv = new ContentValues();
        cv.put(DBTables.TDownloadedPath.PETITION, petition);
        cv.put(DBTables.TDownloadedPath.RESPONSE, response);
        cv.put(DBTables.TDownloadedPath.TIME_EXPIRED, timeExpired);
        return cv;
    }



}
