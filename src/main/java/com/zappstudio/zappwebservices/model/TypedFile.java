package com.zappstudio.zappwebservices.model;

import java.io.File;

/**
 * Created by puesto5 on 18/5/16.
 */
public class TypedFile {

    private Typed mediaType;
    private File file;

    public TypedFile(Typed mediaType, File file) {
        this.mediaType = mediaType;
        this.file = file;
    }

    public Typed getMediaType() {
        return mediaType;
    }

    public void setMediaType(Typed mediaType) {
        this.mediaType = mediaType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
