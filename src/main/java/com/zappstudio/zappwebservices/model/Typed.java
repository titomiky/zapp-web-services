package com.zappstudio.zappwebservices.model;

import okhttp3.MediaType;

public enum Typed {


    TEXT("text"), IMAGE("image"), AUDIO("audio"), VIDEO("video"), APPLICATION("APPLICATION");

    private String value;

    Typed(String aux) {
        this.value = aux;
    }

    public static MediaType getMediaTyped(Typed typed) {
        return MediaType.parse(typed.value);
    }

}