# ZappWebServices

## Development

### Configuracion inicial

Implementando la clase ConfigurationRestClient

Ejemplo:



```java

public class Configuration implements ConfigurationRestClient {

    //Control de errores JSON. Se devuelve el BaseException correspondiente o null si no está contemplado en el análisis de casos.
    @Nullable
    @Override
    public BaseException checkErrorGeneric(JsonElement jsonElement) {
     int code = jsonElement.getAsJsonObject().get("code").getAsInt();
          switch(code){
              case 10: return new ErrorToken();
              default: return null;
          }
    }

    //Control de errores HTTP. Se devuelve el BaseException correspondiente o null si no está contemplado en el análisis de casos.
    @Nullable
    @Override
    public BaseException checkHttpError(int code, @Nullable String body) {
        BaseException baseException;
        switch (code) {
            case CODE_CONTENT_DOES_NOT_EXIT:
                baseException = new ContentDoesNotExistException();
                break;
            case CODE_USER_TOKEN_EXCEPTION:
                baseException = new TokenUserException();
                break;
            default:
                baseException = null;
                break;
        }
        return baseException;
    }
    
    
    //Tratamiento general a la respuesta, sirve para coger datos que vengan en todas las peticiones y tambien preparar el body para el callback de correcto.
    @Override
    public JsonElement extractBodyJsonElement(JsonElement response) {
       JsonElement body = response.getAsJsonObject().get("body");
       return body;
    }
        
    
    //Basepath del servidor al que se van a hacer las peticiones
    @Override
    public String getBasepath() {
        return "http://api.mrbrockman.appcenter.es/";
    }
    
    //Todas las peticiones cogen estas cabeceras
    @Nullable
    @Override
    public Map<String, String> getHeadersInterception() {
        return null;
    }

    //Timeout de las peticiones Tiempo de medida
    @Override
    public TimeUnit getTimeoutTimeUnit() {
        return TimeUnit.SECONDS;
    }

    //Timeout de las peticiones Tiempo
    @Override
    public int getTimeOutTime() {
        return 30;
    }

    //Habilitar o deshabilitar el Log de peticiones
    @Override
    public HttpLoggingInterceptor.Level getLevelLog() {
        return HttpLoggingInterceptor.Level.BODY;
    }

}

```

### Realizar Peticiones

Peticion Get:
```java
   PetitionGet.Builder builder = new PetitionGet.Builder(
                        context,
                        new ConfigurationImpl(context),
                        ENDPOINT_GET_NEWS,
                        new CallbackGeneric() {
                            @Override
                            public void success(JsonElement jsonElement) {
                                //Parse response
                            }

                            @Override
                            public void error(Exception e) {
                                //Callback Error
                            }
                        })
                        .setParam("category", String.valueOf(categoryId))
                        .setParam("page", String.valueOf(page))
                        .build().makePetition();
```

Peticion Post:
```java
   Map<String, String> map = new HashMap<>();
   PetitionFormUrl.Builder builder = new PetitionFormUrl.Builder(
                        context,
                        new Configuration(),
                        ENDPOINT_GET_NEWS,
                        map,
                        new CallbackGeneric() {
                            @Override
                            public void success(JsonElement jsonElement) {
                                //Parse response
                            }

                            @Override
                            public void error(Exception e) {
                                //Callback Error
                            }
                        })
                        .setParam("category", String.valueOf(categoryId))
                        .setParam("page", String.valueOf(page))
                        .build().makePetition();
```


Peticion Post Json:
```java
     JsonObject jsonObject = new JsonObject();
     jsonObject.addProperty(PARAM_EMAIL, email);
     jsonObject.addProperty(PARAM_PASSWORD, password);

     PetitionJson.Builder builder = new PetitionJson.Builder(
                        context,
                        new Configuration(),
                        ENDPOINT_LOGIN,
                        jsonObject,
                        new CallbackGeneric() {
                           @Override
                           public void success(JsonElement jsonElement) {
                                //Parse response
                           }

                           @Override
                           public void error(Exception e) {
                            //Callback Error
                           }
                        })
                        .build().makePetition();
```

### Crear un error custom
Todos los errores deben heredar de la clase BaseException
```sh
public class ErrorToken extends BaseException {
}
```

Control de errores en el base activity
```sh
 public void CallbackError(BaseException base){
 }
```ºº

### Caché
#### Cachear petición
Se añade este método al constructor del builder con los milisegundos de expiración del cacheado:

__.cache(long timeExpired)__

```java
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty(PARAM_EMAIL, email);
    jsonObject.addProperty(PARAM_PASSWORD, password);

    PetitionJson.Builder builder = new PetitionJson.Builder(
        context,
        new Configuration(),
        ENDPOINT_LOGIN,
        jsonObject,
        new CallbackGeneric() {
               @Override
               public void success(JsonElement jsonElement) {
                    //Parse response
               }

               @Override
               public void error(Exception e) {
                //Callback Error
               }
        })
        .cache(Constants.GET_MODELS_EXPIRATION_CACHE)
        .build().makePetition();
```
Válido para cualquiera de los 3 tipos de peticiones (GET, POST y Json).

#### Borrar Caché
Para borrar caché se llama al siguiente método:

__PetitionCache.getInstance().clearCache(Context context);__

Esto eliminará la base de datos de caché por completo.

### Dependencias
com.squareup.retrofit2:retrofit:2.0.0'

com.squareup.okhttp3:okhttp:3.2.0'

com.squareup.retrofit2:converter-gson:2.0.0-beta4'

com.squareup.okhttp3:logging-interceptor:3.0.1'

compile group: 'com.squareup.retrofit2', name: 'converter-gson', version: '2.0.2'

### A tener en cuenta
Vigilar el gson converter para ir actualizandolo
Si se pone el log en HttpLoggingInterceptor.Level.BODY
puede dar desbordamiento de memoria al intentar hacer log de una peticion muy pesada.